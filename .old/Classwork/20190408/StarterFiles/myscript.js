$(document).ready(function(){ //Run the following after the page is ready.
    "use strict";             //Tell jQuery to be strict about the syntax.
    console.log("running");
    $("#submit").click(function(){
        $("h1").css("font-size", $("#h1_size").val() + "rem");
        $("h1").css("font-weight", $("#h1_weight").val());
        $("h2").css("font-size", $("#h2_size").val() + "rem");
        $("h2").css("font-weight", $("#h2_weight").val());
        console.log($("#h1_size").val());
    });
})
