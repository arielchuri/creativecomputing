$(document).ready(function(){ //Run the following after the page is ready.
    "use strict";             //Tell jQuery to be strict about the syntax.
    console.log("running");
    
    document.getElementById("navMenu").innerHTML =
    '<ul class="nav">'+
    '<li class="nav"><a href="index.html">Home</a></li>'+
    '<li class="nav"><a href="portfolio.html">Portfolio</a></li>'+
    '<li class="nav"><a href="about.html">About</a></li>'+
    '</ul>';
})
