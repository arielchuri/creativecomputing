# Creative Computing
## Notes
### Session 01
The document you are looking at is composed of a series of characters I have typed on my computer.
The series of characters that make up the headline of this section are the same as the ones you are reading now but I have indicated to the computer that those characters are a headline.
This helps the reader establish a hierarchy of information.
Lets look at the characters I actually typed rather than just the ones you see in the web browser:

     <h1>Creative Computing</h1>
     <h2>Notes</h2>
     <h3>Session 01</h3>
     <p>
        The document you are looking at is composed of a series of characters I have typed on my computer.
        The series of characters that make up the headline of this section are the same as the ones you are reading now but I have indicated to the computer that those characters are a headline.
        This helps the reader establish a hierarchy of information.
        Lets look at the characters I actually typed rather than just the ones you see in the web browser:
     </p>


**Text** can be _formatted_.
